<?php 
	require "connection.php";



	function validate_form(){
		// validation logic
		// well check if each of the fields in the form has a value.
		// if not, will increase the $error total > 0, will return false
		// well check if the file extension of the image is within the acceptable file extensions
		// if not, will increase the $error total and if the $error total > 0, will return false 
		$error = 0;
		$file_types = ["jpg","jpeg","png", "gif", "bmp","svg"];
		$file_ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));

		if ($_POST['name']=="" || !isset($_POST['name'])) {
			$errors++;
		};
		if ($_POST['price']<=0 || !isset($_POST['price'])) {
			$errors++;
		};
		if ($_POST['description']=="" || !isset($_POST['description'])) {
			$errors++;
		}
		if ($_POST['category_id']== null || !isset($_POST['category_id'])) {
			$errors++;
		};
		if ($_FILES['image']['name'] != "" ) {
			if(!in_array($file_ext, $file_types)){
				$errors++;
			};
		};

		if($error > 0){
			return false;
		}else{
			return true;
		};
	};
	if(validate_form()){
		// Process of saving an item 
		// 1. capture all data from form through $_POST or $_FILES for image
		// 2. move uploaded image file to the assets/images directory
		// 3. Create the query
		// 4. use mysqli_query
		// 5. Go back to catalog if successful
		// 6. If unsuccessful go back to add item form
		$id = $_POST['id'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$description =$_POST['description'];
		$category_id = $_POST['category_id'];

		$item_query = "SELECT image FROM items WHERE id = $id"; 
		$image_result = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

		$image = "";
		// check if a user uploaded a new image
		// if not, save the old value of image id $image
		// if yes do the process of moving files
		if ($_FILES['image']['name'] == "") {
			$image = $image_result['image'];
		}else{

			$destination = "../assets/images/";
			$file_name = $_FILES['image']['name'];
			move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);

			$image = $destination.$file_name;
		}
		

		

		$update_item_query = "UPDATE items SET name = '$name', price = $price, description = '$description', image = '$image', category_id = $category_id WHERE id = $id";
		$result = mysqli_query($conn, $update_item_query);

		header("Location: ../views/catalog.php"); 
		}else{
			header("Location: ". $_SERVER['HTTP_REFERER']);
		}
 ?>