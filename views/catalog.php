<?php 

	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
	// need always for any database connection 
?>
	<h1 class="text-center py-5">Catalog Page</h1>
	<div class="container">
		<div class="row">
			<?php 
				// steps for retrieval of items
				//1. Create a query 
				//2. use mysqli_query to get the results
				//3. if array use foreach
				//4. if object use mysqli_fetch_assoc to convert the data to associative array
				//	4.1 use foreach ($result as $key => $value)
				$items_query = "SELECT * FROM items";
				$items = mysqli_query($conn, $items_query);
				

				foreach($items as $indiv_item){
			?>
				<div class="col-lg-4 py-2">
					<div class="card h-100">
						<img src="<?php echo $indiv_item['image'] ?>" class="card-img-top" alt="image">
						<div class="card-body">
							<h4 class="card-title"><?php echo $indiv_item['name']?></h4>
							<p class="card-text">Php <?php echo $indiv_item['price'] ?>.00</p>
							<p class="card-text"><?php echo $indiv_item['description']?></p>
							<?php
								 //process of displaying category
								 // 1. get the category name where id is equal $indiv_item['category_id']
								 // 2. display the data
								 $catId = $indiv_item['category_id'];
								 $category_query = "SELECT * FROM categories WHERE id = $catId";
								 // since $category_query is an object, change it into assoc array by mysqli_fetch_assoc
								 $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
								 // if assoc array 
								
							?>
								<p class="card-text">Category: <?php echo $category['name'] ?></p>
							
						</div>
						<div class="card-footer">
							<a href="edit-item-form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-secondary">EDIT ITEM</a>
						</div>
					</div>
				</div>
			<?php 
				}
			 ?>
		</div>
	</div>
<?php		
	}
 ?>