<?php 
	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
?>	
	<h1 class="text-center py-5">EDIT ITEM FORM</h1>
	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<!-- enctype because we need to upload -->
			<form action="../controllers/process-edit-item.php" method="POST" enctype="multipart/form-data">
				<?php 
					$item_id = $_GET['id'];

					$item_query = "SELECT * FROM items WHERE id = $item_id";
					$item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

					
				 ?>
				<div class="form-group">
					<label for="name">Item Name</label>
					<input type="text" name="name" class="form-control" value="<?php echo $item['name']?>">
				</div>
				<div class="form-group">
					<label for="price">Price</label>
					<input type="number" name="price" class="form-control" value="<?php echo $item['price']?>">
				</div>
				<div class="form-group">
					<label for="description">Item Description:</label>
					<textarea name="description" class="form-control"><?php echo $item['description']?></textarea>
				</div>
				<div class="form-group">
					<label for="image">Image: </label>
					<img src="<?php echo $item['image']?>" height="50px" width="50px">
					<input type="file" name="image" class="form-group">
				</div>
				<div class="form-group">
					<label for="category_id">Item Category</label>
					<select name="category_id" class="form-control">
						<!-- we need to get all categories from the field to be userfrienly -->
						<?php 
							$category_query="SELECT * FROM categories";
							$categories = mysqli_query($conn, $category_query);
							// to get each of the item inside categories table in database
							foreach ($categories as $indiv_category) {
						?>
							<option 
							value="<?php echo $indiv_category['id']?>"
							<?php echo $indiv_category['id']==$item['category_id'] ? 
							"selected": ""?>
							><?php echo $indiv_category['name'] ?></option>
						<?php		
							}
						 ?>
					</select>
				</div>
				<input type="hidden" value="<?php echo $item['id']?>" name="id">
				<button type="submit" class="btn btn-success">EDIT ITEM</button>
			</form>
		</div>
	</div>
<?php		
	}
 ?>